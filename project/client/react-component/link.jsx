
'use strict';

import {RouterContext} from './router-context';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class Link extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  _onClickLink(e) {
    if(e.defaultPrevented) {
      return;
    }
    if(this.props.onClick) {
      return this.props.onClick(e);
    }
    if(e.metaKey || e.ctrlKey || e.shiftKey) {
      return;
    }
    e.stopPropagation();
    e.preventDefault();
    let replace = this.props.replace ? this.props.replace : false;
    if(this.props.replaceStay && this.props.href.startsWith(this.context.routeUriPart)) {
      replace = true;
    }
    this.context.history(this.props.href, {
      replace: replace,
      global: this.props.global ? this.props.global : false
    }, {});
  }
 
  render() {
    const role = this.props.onClick ? {role: 'button'} : {};
    const className = this.props.className ? {className: this.props.className} : {};
    const href = this.props.href;
    const uri = href ? (this.props.global ? href : `${this.context.previousUriPart}${href.startsWith('/') ? '' : (href ? '/' : '')}${href}`) : undefined;
    const dataName = this.props['data-name'] ? {['data-name']: this.props['data-name']} : {};
    return (
      <a {...className} href={uri} {...role} {...dataName} data-is-link onClick={(e) => {
          this._onClickLink(e);
        }}>
       {this.props.children}
      </a>
    );
  }
}


Link.contextType = RouterContext;
