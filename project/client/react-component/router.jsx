
'use strict';

import {RouterContext} from './router-context';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class Router extends ReactComponentBase {
  constructor(props) {
    super(props, {
      name: 'RouterContext',
      previousUriPart: '',
      routeUriPart: '',
      restUriPart: '',
      location: {
        hash: document.location.hash,
        host: document.location.host,
        hostname: document.location.hostname,
        href: document.location.href,
        origin: document.location.origin,
        pathname: document.location.pathname,
        port: document.location.port,
        protocol: document.location.protocol,
        search: document.location.search
      },
      historyData: {
        type: Router.HISTORY_POP,
        options: null,
        state: null
      },
      self: null
    });
    this.state.self = this;
    this.myContext.history = (href, options, state) => {
      this._updateRouterStateAndHistory(href, options.global, options, state);
    };
    const calculatedLocation = this._calculateRouterState(window.location);
    if(null !== calculatedLocation) {
      this.state.routeUriPart = calculatedLocation.routeUriPart;
      this.state.restUriPart = calculatedLocation.restUriPart;
      this.state.history = (href, state, options) => {
        this.myContext.history(href, state, options);
      };
    }
    this.onPopState = this._onPopState.bind(this);
    this.childRoute = false;
    this.rerenderCb = null;
  }
  
  didMount() {
    window.addEventListener('popstate', this.onPopState, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state, nextState)
      || !this.shallowCompare(this.props, nextProps);
  }
  
  didUpdate(prevProps, prevState) {
    if(Router.HISTORY_PUSH === this.state.historyData.type) {
      if(document.location.href !== this.state.location.href) {
        if(this.state.historyData.options.replace) {
          history.replaceState(this.state.historyData.state, '', this.state.location.href);
        }
        else {
          history.pushState(this.state.historyData.state, '', this.state.location.href);
        }
      }
      if(this.rerenderCb) {
        setTimeout((self) => {
          self.rerenderCb();
          self.rerenderCb = null;
        }, 0, this);
      }
    }
  }
  
  willUnmount() {
    window.removeEventListener('popstate', this.onPopState, true);
  }
    
  rerender(uri, cb) {
    this.rerenderCb = cb;
    this._updateRouterStateAndHistory(uri, false, {replace: false}, null);
  }
  
  onSwitch(routeUriPart) {
    this.childRoute = true;
  }
  
  _onPopState(e) {
    e.stopPropagation();
    e.preventDefault();
    const calculatedLocation = this._calculateRouterState(document.location, false);
    if(null !== calculatedLocation) {
      this.updateState({
        routeUriPart: {$set: calculatedLocation.routeUriPart},
        restUriPart: {$set: calculatedLocation.restUriPart},
        location: {$set: calculatedLocation.location},
        historyData: {
          type: {$set: Router.HISTORY_POP}
        }
      });
    }
  }
  
  _calculateRouterState(data, global) {
    if('string' === typeof data) {
      if(global) {
        data = `${this.state.routeUriPart}${data}`
      }
      data = new URL(data);
    }
    const routeUriPart = `${data.protocol}//${data.host}`;
    const restUriPart = data.pathname;
    const href = data.href;
    if(this.state.routeUriPart !== routeUriPart || this.state.restUriPart !== restUriPart || this.state.href !== href) {
      return {
        routeUriPart,
        restUriPart,
        location: {
          hash: data.hash,
          host: data.host,
          hostname: data.hostname,
          href: data.href,
          origin: data.origin,
          pathname: data.pathname,
          port: data.port,
          protocol: data.protocol,
          search: data.search
        }
      };
    }
    return null;
  }
    
  _updateRouterStateAndHistory(data, global, options, state) {
    const calculatedLocation = this._calculateRouterState(data, global);
    if(null !== calculatedLocation) {
      this.updateState({
        routeUriPart: {$set: calculatedLocation.routeUriPart},
        restUriPart: {$set: calculatedLocation.restUriPart},
        location: {$set: calculatedLocation.location},
        historyData: {
          type: {$set: Router.HISTORY_PUSH},
          options: {$set: options},
          state: {$set: state}
        }
      });
    }
  }
  
  render() {
    this.childRoute = false;
    return (
      <RouterContext.Provider value={this.state}>
        {this.props.children}
      </RouterContext.Provider>
    );
  }
}


Router.HISTORY_POP = 0;
Router.HISTORY_PUSH = 1;
