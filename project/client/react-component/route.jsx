
'use strict';

import {RouterContext} from './router-context';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class Route extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.router = {
      name: 'RouterContext',
      previousUriPart: '',
      routeUriPart: '',
      restUriPart: '',
      location: {
        hash: '',
        host: '',
        hostname: '',
        href: '',
        origin: '',
        pathname: '',
        port: '',
        protocol: '',
        search: ''
      },
      self: null
    }
    this.state.self = this;
    this.refCatchClick = React.createRef();
    this.onClick = this._onClick.bind(this);
    this.childRoute = false;
  }
  
  didMount() {
    if(this.props.catchClick && this.refCatchClick.current) {
      this.refCatchClick.current.addEventListener('click', this.onClick, false);
    }
  }
    
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state, nextState)
      || !this.shallowCompare(this.props, nextProps);
  }
  
  willUnmount() {
    if(this.props.catchClick && this.refCatchClick.current) {
      this.refCatchClick.current.removeEventListener('click', this.onClick, false);
    }
  }
  
  onSwitch(routeUriPart) {
    this.childRoute = true;
  }
  
  renderRouterContext(childProps) {
    this.childRoute = false;
    return (
      <RouterContext.Provider value={this.router}>
        {React.createElement(this.props.handler, childProps, this.children)}
      </RouterContext.Provider>
    );
  }
  
  renderHandler(childProps) {
    if(this.props.handler) {
      if(this.props.catchClick) {
        return (
          <div ref={this.refCatchClick}>
            {this.renderRouterContext(childProps)}
          </div>
        );
      }
      else {
        return (
          <>
            {this.renderRouterContext(childProps)}
          </>
        );
      }
    }
    else {
      return null;
    }
  }
  
  render() {
    let props = null;
    const parentRestUriPart = this.context.restUriPart;
    if(!this.props.notFound) {
      if(!Array.isArray(this.props.switch)) {
        if(this.props.switch) {
          if(this.props.switch === parentRestUriPart) {
            props = {_: ''};
          }
          else {
            props = this._matchUrl(this.props.switch, parentRestUriPart);
          }
        }
        else if(!parentRestUriPart) {
          props = {_: ''};
        }
      }
      else {
        const conditions = this.props.switch;
        for(let i = 0; i < conditions.length; ++i) {
          const currentSwitch = conditions[i];
          if(currentSwitch) {
            if(currentSwitch === parentRestUriPart) {
              props = {_: ''};
            }
            else {
              props = this._matchUrl(currentSwitch, parentRestUriPart);
            }
          }
          else if(!parentRestUriPart) {
            props = {_: ''};
          }
          if(props) {
            break;
          }
        }
      }
    }
    
    if(props || this.props.notFound) {
      const context = this.context;
      if(this.props.notFound && context.self.childRoute) {
        return null;
      }
      const parentPreviousUriPart = context.previousUriPart;
      const parentRouteUriPart = context.routeUriPart;
      const previousUriPart = parentPreviousUriPart + parentRouteUriPart;
      const restUriPart = props?._ ?  props._ : '';
      const routeUriPart = parentRestUriPart.substring(0, parentRestUriPart.length - restUriPart.length);
      if(this.router.routeUriPart !== routeUriPart || this.router.restUriPart !== restUriPart || this.router.location !== context.location) {
        this.router = {
          name: 'RouterContext',
          previousUriPart,
          routeUriPart,
          restUriPart,
          location: context.location,
          self: this,
          history: (href, options, state) => {
            const _options = options ? options : {
              replace: false,
              replaceStay: false,
              global: false
            };
            if(!_options.replace) {
              _options.replace = false;
            }
            if(!_options.replaceStay) {
              _options.replaceStay = false;
            }
            if(!_options.global) {
              _options.global = false;
            }
            const __sep = href.startsWith('/') ? '' : (_options.noSlash ? '' : '/');
            const __previousUriPart = _options.noSlash ? (this.router.previousUriPart.endsWith('/') ? this.router.previousUriPart.substring(0, this.router.previousUriPart.length - 1) : this.router.previousUriPart) : this.router.previousUriPart;
            const uri = options && options.global ? href : `${__previousUriPart}${__sep}${href}`;
            this.myContext.history(uri, _options, state ? state : {});
          },
          attachHistory: (action) => {
            action.setHistoryObject(this.router);
          }
        };
      }
      
      this.context.self.onSwitch(this.router.routeUriPart);
      const childProps = {};
      Reflect.set(childProps, 'location', this.router.location);
      Reflect.ownKeys(this.props).forEach((ownKey) => { 
        if('handler' !== ownKey && 'switch' !== ownKey && 'notFound' !== ownKey) {
          Reflect.set(childProps, ownKey, Reflect.get(this.props, ownKey));
        }
      });
      if(props) {
        Reflect.ownKeys(props).forEach((ownKey) => {
          if('_' === ownKey) {
            Reflect.set(childProps, '_uriPath', this.router.restUriPart);
          }
          else if(!ownKey.startsWith('_')) {
            Reflect.set(childProps, ownKey, Reflect.get(props, ownKey));
          }
        });
      }
      return (
        <>
          {this.renderHandler(childProps)}
        </>
      );
    }
    else {
      return null;
    }
  }
  
  _onClick(e) {
    if(e.defaultPrevented) {
      return;
    }
    if(e.metaKey || e.ctrlKey || e.shiftKey) {
      return;
    }
    let element = e.target;
    while(element && element.nodeName !== 'A') {
      element = element.parentNode;
    }
    if(!element) {
      return;
    }
    if(element.dataset['isLink']) {
      return;
    }
    if(element.target && element.target !== '_self') {
      return;
    }
    if(element.attributes.download) {
      return;
    }
    if(this.router.location.origin !== e.target.origin) {
      return;
    }
    e.stopPropagation();
    e.preventDefault();
    this.myContext.history(element, {
      replace: this.props.replace ? this.props.replace : false,
      global: this.props.global ? this.props.global : false
    }, {});
  }
  
  _matchProperties(properties) {
    if(0 === properties.length || undefined === properties[0].value) {
      return null;
    }
    else {
      const props = {};
      for(let i = 0; i < properties.length; ++i) {
        const property = properties[i];
        if(undefined === property.value) {
          return props;
        }
        Reflect.set(props, property.name, property.value);
      }
      return props;
    }
  }
  
  _matchUrl(switchUrl, url) {
    const switchUrlParts = switchUrl.split('/');
    const urlParts = url.split('/');
    const minLenght = Math.min(switchUrlParts.length, switchUrlParts.length);
    const properties = [];
    for(let i = 0; i < minLenght; ++i) {
      const switchUrlPart = switchUrlParts[i];
      const urlPart = urlParts[i];
      const endingStar = switchUrlPart.endsWith('*');
      if(switchUrlPart.startsWith(':')) {
        let index = 0;
        let urlPopIndex = 0;
        const indexDatas = [];
        do {
          const sepIndex = switchUrlPart.indexOf('.', index + 1);
          const indexData = {
            propIndex: index,
            sepIndex: sepIndex,
            urlPropIndex: urlPopIndex,
            urlSepIndex: urlPart ? urlPart.length : -1
          }
          indexDatas.push(indexData);
          if(-1 == sepIndex) {
            break;
          }
          else {
            urlPopIndex = urlPart ? urlPart.indexOf('.', urlPopIndex) + 1 : -1;
            indexData.urlSepIndex = urlPopIndex;
          }
          index = switchUrlPart.indexOf(':', index + 1);
        } while(-1 !== index);
        indexDatas.forEach((indexData) => {
          const nameStart = indexData.propIndex + 1;
          const nameStop = -1 !== indexData.sepIndex ? indexData.sepIndex : (endingStar ? switchUrlPart.length - 1 : switchUrlPart.length);
          const valueStart = indexData.urlPropIndex;
          const valueStop = indexData.urlSepIndex;
          properties.push({
            name: switchUrlPart.substring(nameStart, nameStop),
            value: urlPart?.substring(valueStart, valueStop)
          });
        });
      }
      if(endingStar) {
        const path = switchUrlPart.substring(0, switchUrlPart.length - 1);
        if(path !== urlParts[i] && 0 === properties.length) {
          return null;
        }
        else {
          const result = this._matchProperties(properties);
          let restPath = urlParts.slice(i + 1).join('/');
          restPath = restPath ? '/' + restPath : '';
          if(result) {
            Reflect.set(result, '_', restPath);
            return result;
          }
          else {
            return {
              _: restPath
            };
          }
        }
      }
    }
    return this._matchProperties(properties);
  }
}


Route.contextType = RouterContext;
